mxA51
=====

is a short form of ***M***ustang***X*** ***A***rea ***51***

this **IS OUR TESTING GROUND** for building the *MustangX CMS* from scratch.


## Tutorial we are following

https://www.youtube.com/watch?v=u4-aItfZQuo&index=1&list=PLAkMqlQoeMeiwvNWpe3mhgQxAa1jiGwmt

***Developing a Dynamic Website 2014***


----
~~https://www.youtube.com/watch?annotation_id=annotation_2856964881&feature=iv&list=PLnWrVWj1APWsYsc1tQoIjraaB10CRsjBr&src_vid=b95bJtgCTog&v=b95bJtgCTog~~

***Building a CMS with PHP***


----
~~https://www.youtube.com/watch?v=NhoZMxRbCwU~~

CMS from scratch.

***Creating an Advance CMS From Scratch like Wordpress with admin and more***

|--> Real Nice!!! Problem - Tut not finished <--|


----
~~https://www.codeofaninja.com/2014/06/php-object-oriented-crud-example-oop.html~~

-->Written Tutorial. Uses PDO and OOP. The code look like a great substance to expand upon.<--

----


~~https://www.youtube.com/watch?v=eZUug-WdIFc&list=PLbA_Q7ZFvaeAYYBrzgIeY91z44eICYKKh~~

-->Video Tutorial. Absolute boring. NOT beginner friendly. Code is good - video very poor quality.<--

----
----

this does not mean that we will follow all the way throught if a blocker is found, the above link will get a 'strike through' and moved down the list. This is done to avoid duplicating the already used tutorial.

### Planed (wanted) Feature
* Multi language support
* Widgets
* Plugins
* Themes
* Auto installer
* Fast & secure
* View statistics
* Dynamic pages
* News articles
* Recaptcha
* Blacklist
* Badword list
* EU Cookie Law conform
* Legal Pages
