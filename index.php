<?php include('config/setup.php'); ?>

<!DOCTYPE html>
<html lang="en">

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <?php include('config/css.php'); ?>
    <?php include('config/js.php'); ?>

    <link rel="icon" href="images/favicon.png">

    <title><?php echo $page_title.' | '.$site_name; ?></title>

  </head>
  <body>
<div id="wrap">
    <nav class="navbar navbar-expand-lg navbar-light bg-light" role="navigation">

      <!--<a class="navbar-brand" href="/"><img src="images/logo.png"></img></a>-->

<!-- BEGIN NAV -->
    <div class="container container-fluid">
      <ul class="nav navbar-nav">
        <li class="nav-item active"><a class="nav-link" href="/">Home</a></li>
        <li class="nav-item"><a class="nav-link" href="#">About</a></li>
        <li class="nav-item"><a class="nav-link" href="#">FAQ</a></li>
        <li class="nav-item"><a class="nav-link" href="#">Contact</a></li>
      </ul>
    </div>
    </nav>

<!-- END NAV -->

<!-- CONTENT START -->
<div class="container container-fluid">
  <h1>Content Area</h1>
</div>
<!-- CONTENT END -->

<!-- SIDEBAR BEGIN -->

<!-- SIDEBAR END -->


</div> <!-- End WRAP --><!--|| This is impotant ||-->


<!-- FOOTER BEGIN -->
 <footer id="footer">
   <div class="container">
     <p>
       <span class="text-muted">
         <a href="http://mustangx.org"
         target="_blank"><img src="images/footer_logo.png" /></a>  &copy; 2016-<?php echo date('Y'); ?> the <a href="http://mustangx.org"
         target="_blank">MustangX Project</a> and it's contibutors.
       </span>
     </p>
   </div>
 </footer>
<!-- FOOTER END -->



  </body>
</html>
